package com.difusal.snake;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class UserLogin extends AppCompatActivity {

    public final static String EXTRA_MESSAGE = "MESSAGE";
    private ListView obj;
    DBHelper mydb;
    EditText username;
    EditText name;
    EditText email;
    EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_login);
        mydb = new DBHelper(this);
        username = (EditText)  findViewById(R.id.loginUsername);
        password = (EditText)  findViewById(R.id.loginPassword);
    }

    public void run(View view){

        String valUsername = username.getText().toString();
        String valPassword = password.getText().toString();
        Cursor takenUser = mydb.getUser(valUsername, valPassword);
        if(takenUser.getCount() == 1){
            Toast.makeText(getApplicationContext(), "Logged In", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(getApplicationContext(),MainActivity.class);
            startActivity(intent);
        }else{
            Toast.makeText(getApplicationContext(), "Wrong Username/Password or Not Registered", Toast.LENGTH_SHORT).show();
        }
    }

    public void registerRun(View view) {

        Toast.makeText(getApplicationContext(), "Logged In", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(getApplicationContext(), UserRegisteration.class);
        startActivity(intent);
    }

}
