package com.difusal.snake;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "MyDBName.db";
    public static final String CONTACTS_TABLE_NAME = "user";
    public static final String CONTACTS_COLUMN_ID = "username";
    public static final String CONTACTS_COLUMN_NAME = "nama";
    public static final String CONTACTS_COLUMN_EMAIL = "email";
    public static final String CONTACTS_COLUMN_PASSWORD = "password";
    private HashMap hp;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        String createTableFormat = new MessageFormat("create table {0} ({1} text primary key, {2} text,{3} text,{4} text)").format(new String[]{CONTACTS_TABLE_NAME, CONTACTS_COLUMN_ID, CONTACTS_COLUMN_NAME, CONTACTS_COLUMN_EMAIL, CONTACTS_COLUMN_PASSWORD});
        db.execSQL(createTableFormat);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL(new MessageFormat("DROP TABLE IF EXISTS {0}").format(new String[]{CONTACTS_TABLE_NAME}));
        onCreate(db);
    }

    public Cursor getUser(String username, String password){
        SQLiteDatabase db = this.getReadableDatabase();
        String rawQuery = "SELECT * FROM user where username='"+username+"' and password='"+password+"'";
        Cursor res = db.rawQuery(rawQuery, null);
        return res;
    }

    public boolean insertUser(String username, String name, String email, String password) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        try{
            contentValues.put(CONTACTS_COLUMN_ID, username);
            contentValues.put(CONTACTS_COLUMN_NAME, name);
            contentValues.put(CONTACTS_COLUMN_EMAIL, email);
            contentValues.put(CONTACTS_COLUMN_PASSWORD, password);
            db.insert(CONTACTS_TABLE_NAME, null, contentValues);
            return true;
        } catch (Exception e){
            return false;
        }
    }
}

