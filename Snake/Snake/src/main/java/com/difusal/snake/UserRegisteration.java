package com.difusal.snake;

;
import android.content.Intent;
import android.os.Bundle;

import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class UserRegisteration extends AppCompatActivity {
    public final static String EXTRA_MESSAGE = "MESSAGE";
    private ListView obj;
    DBHelper mydb;
    EditText username;
    EditText name;
    EditText email;
    EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_registeration);
        mydb = new DBHelper(this);
        username = (EditText)  findViewById(R.id.editTextUsername);
        name = (EditText)  findViewById(R.id.editTextName);
        email = (EditText)  findViewById(R.id.editTextEmail);
        password = (EditText)  findViewById(R.id.editTextPassword);
    }

    public void run(View view){

        String valUsername = username.getText().toString();
        String valName = name.getText().toString();
        String valEmail = email.getText().toString();
        String valPassword = password.getText().toString();

        if(mydb.insertUser(valUsername, valName, valEmail, valPassword)){
            Toast.makeText(getApplicationContext(), "Saved", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(getApplicationContext(),UserLogin.class);
            startActivity(intent);
        }else{
            Toast.makeText(getApplicationContext(), "Not Saved", Toast.LENGTH_SHORT).show();
        }
    }

}

